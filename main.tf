# Configure the AWS Provider
provider "aws" {
  region  = "ap-southeast-1"
  #access_key = "AKIAQ6OVFGCGZKKRFU7X"
  #secret_key = "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"
}

variable "cidr-blocks" {
  description = "List of objects"
  type = list(object({
              cidr-block = string 
              name = string}
              )
  )
}

resource "aws_vpc" "development-vpc" {
  cidr_block       = var.cidr-blocks[0].cidr-block
  instance_tenancy = "default"
   
  tags = {
  Name = var.cidr-blocks[0].name
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block = var.cidr-blocks[1].cidr-block
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = var.cidr-blocks[1].name
  }
}

# #
# data "aws_vpc" "existing-vpc" {
# #  default = true
# #}


resource "aws_subnet" "dev-subnet-2" {
  vpc_id = aws_vpc.development-vpc.id
  cidr_block =   var.cidr-blocks[2].cidr-block
  availability_zone = "ap-southeast-1a"

  tags = {
     Name =   var.cidr-blocks[2].name
  }
}

output "dev-vpc-id" {
   value = aws_vpc.development-vpc.id
}

output "subnet-id" {
   value = aws_subnet.dev-subnet-2.id
}

# # resource "aws_vpc" "prod-vpc" {
# #   cidr_block       = "10.0.0.0/16"
# #   instance_tenancy = "default"

# #   tags = {
# #     Name = "Production"
# #   }
# # }

# # resource "aws_internet_gateway" "gw" {
# #   vpc_id = aws_vpc.prod-vpc.id

# #   tags = {
# #     Name = "Prod Internet Gateway"
# #   }
# # }

# # resource "aws_route_table" "prod-route-table" {
# #   vpc_id = aws_vpc.prod-vpc.id

# #   route {
# #     cidr_block = "0.0.0.0/0"
# #     gateway_id = aws_internet_gateway.gw.id
# #   }

# #   route {
# #     ipv6_cidr_block        = "::/0"
# #     gateway_id = aws_internet_gateway.gw.id
# #   }

# #   tags = {
# #     Name = "Prod"
# #   }
# # }

# # resource "aws_subnet" "subnet-1" {
# #   vpc_id = aws_vpc.prod-vpc.id
# #   cidr_block = "10.0.1.0/24"
# #   availability_zone = "ap-southeast-1a"

# #   tags = {
# #     Name = "Prod-subnet"
# #   }
# # }

# # resource "aws_route_table_association" "a" {
# #   subnet_id      = aws_subnet.subnet-1.id
# #   route_table_id = aws_route_table.prod-route-table.id
# # }

# # resource "aws_security_group" "allow_web" {
# #   name        = "allow_web_traffic"
# #   description = "Allow Web traffic"
# #   vpc_id      = aws_vpc.prod-vpc.id

# #   ingress {
# #     description = "HTTPS from VPC"
# #     from_port   = 443
# #     to_port     = 443
# #     protocol    = "tcp"
# #     cidr_blocks = ["0.0.0.0/0"]
# #   }

# # ingress {
# #     description = "HTTP from VPC"
# #     from_port   = 80
# #     to_port     = 80
# #     protocol    = "tcp"
# #     cidr_blocks = ["0.0.0.0/0"]
# #   }

# #   ingress {
# #     description = "SSL from VPC"
# #     from_port   = 22
# #     to_port     = 22
# #     protocol    = "tcp"
# #     cidr_blocks = ["0.0.0.0/0"]
# #   }

# #   egress {
# #     from_port   = 0
# #     to_port     = 0
# #     protocol    = "-1"
# #     cidr_blocks = ["0.0.0.0/0"]
# #   }

# #   tags = {
# #     Name = "allow_web"
# #   }
# # }

# resource "aws_network_interface" "web-server-nic" {
#   subnet_id       = aws_subnet.subnet-1.id
#   private_ips     = ["10.0.1.50"]
#   security_groups = [aws_security_group.allow_web.id]

# }

# resource "aws_eip" "one" {
#   vpc                       = true
#   network_interface         = aws_network_interface.web-server-nic.id
#   associate_with_private_ip = "10.0.1.50"
#   depends_on = [aws_internet_gateway.gw]
    
# }

# output "server_public_ip" {
#    value = aws_eip.one.public_ip
# }

# resource "aws_instance" "web-server-instance" {
#   ami           = "ami-01581ffba3821cdf3"
#   instance_type = "t2.micro"
#   availability_zone = "ap-southeast-1a"
#   key_name = "main-key"

#   network_interface {
#     device_index = 0
#     network_interface_id = aws_network_interface.web-server-nic.id
#   }

#     user_data = <<-EOF
#                 sudo apt update -y
#                 sudo apt install apache2 -y
#                 sudo systemctl start apache2
#                 sudo bash -c echo "Your very first web server > /var/www/html/index.html"
#                 EOF
#
#   }